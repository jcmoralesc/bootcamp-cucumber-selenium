package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import sun.font.TrueTypeFont;

public class AuthenticationSteps {

    // Configuración Webdriver de Selenium
    private WebDriver driver;
    private ChromeOptions options = new ChromeOptions();


    @Given("^the user is in the login page$")
    public void navigate_login_page() throws Exception {
        System.out.println("Navegar a Login");
        System.setProperty("webdriver.gecko.driver", "./src/test/resources/drivers/chromedriver.exe");

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        //Navegar a la página de Login
        driver.get("https://opensource-demo.orangehrmlive.com/");
        //Muy importante: ponemos Chrome en modo HEADLESS
        //options.setHeadless(true);
    }

    @When("^the user fills the login form$")
    public void fill_loginform() throws Exception {
        System.out.println("Fill login form");

        WebElement userInput = driver.findElement(By.id("txtUsername"));
        WebElement passInput = driver.findElement(By.id("txtPassword"));

        userInput.sendKeys("Admin");
        passInput.sendKeys("admin123");
    }

    @Then("^the user sees your profile$")
    public void user_sees_profile () throws  Exception{
        System.out.println("User sees profile");
    }

}
